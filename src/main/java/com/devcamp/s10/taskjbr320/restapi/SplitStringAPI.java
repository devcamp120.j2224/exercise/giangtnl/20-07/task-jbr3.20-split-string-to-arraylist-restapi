package com.devcamp.s10.taskjbr320.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> splitstringapi(){
        ArrayList<String> result = new ArrayList<String>();
        String str = "we are the champions";
        String[] strArray = str.split("\\s");
        for (int i = 0; i < strArray.length; i++) {
            result.add(strArray[i]);
          }
        return result;  
    }
}
